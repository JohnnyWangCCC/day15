### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	Today we started the simulation project, our teacher introduced the rules in the simulation project, then we were asked to come up with the team name and select the various roles in the project, then the agile activity was explained and we were shown the next schedule. And we were given an explanation of the theme of this time, and we went to give an elevator speech; in the afternoon, we had an introduction and demonstration of the User Journey of the simulation project, the use of Story card, Kanban Board, and how to use CI and CD in the simulation project. and at the end of the day, we had a retro for the week. this time, the teamwork was very successful, and of course, the credit is due to each and every person in the team. credit goes to every member of the team.

### R (Reflective): Please use one word to express your feelings about today's class.

​	Combative.

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	Today's simulation project allowed us to really experience the startup of a project and feel the process of agile development. After today's team activity, we left the shadow of failure behind and regained our confidence, and we all performed well in the team activity. It is worth to think that teamwork can make a huge transformation before and after, which shows the importance of teamwork.

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	The idea of teamwork and communication is deeply embedded into future work, great team transformation comes from team cohesion.
